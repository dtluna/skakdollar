class TranslationsController < ApplicationController
  before_action :set_translation, only: [:show, :destroy]
  before_filter :check_for_admin, only: :destroy
  before_filter :authenticate_user!, only: :my
  before_filter :build_translation, only: :create
  before_filter :set_my_translations, only: :my
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_translation

  # GET /translations
  # GET /translations.json
  def index
    @translations = Translation.order(:created_at).reverse_order.page(params[:page])
  end

  # GET /translations/1
  # GET /translations/1.json
  def show
  end

  # GET /translations/new
  def new
    @translation = Translation.new
  end

  # POST /translations
  # POST /translations.json
  def create
    respond_to do |format|
      if @translation.save
        flash[:success] = 'Перевод создан успешно.'
        format.html { redirect_to @translation }
        format.json { render :show, status: :ok, location: @translation }
      else
        flash[:danger] = 'Что-то пошло не так при создании перевода.'
        format.html { render :new }
        format.json { render json: @translation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /translations/1
  # DELETE /translations/1.json
  def destroy
    @translation.destroy
    respond_to do |format|
      flash[:success] = 'Перевод удалён успешно.'
      format.html { redirect_to translations_url }
      format.json { head :no_content }
    end
  end

  def my
  end

  private

    def check_for_admin
      if not current_user.admin
        logger.error 'Attempt to delete translation #{params[:id]} with no admin permissions'
        flash[:danger] = 'Вы не авторизованы для удаления переводов!'
        redirect_to translations_path
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_translation
      @translation = Translation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def translation_params
      params.require(:translation).permit(:original)
    end

    def invalid_translation
      logger.error 'Attempt to access invalid translation #{params[:id]}'
      flash[:danger] = 'Такого перевода нет!'
      redirect_to translations_path
    end

    def build_translation
      @translation = Translation.new(translation_params)
      @translation.user = current_user
    end

    def set_my_translations
      @translations = current_user.translations.order(:created_at).reverse_order.page(params[:page])
    end
end
