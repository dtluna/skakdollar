module TranslationsHelper
  def translation_user_email
    if @translation.user
      @translation.user.email
    else
      'Гость'
    end
  end
end
