class Translation < ActiveRecord::Base
  include BlondieConversions
  validates :original, presence: true, uniqueness: true
  before_save :translate
  belongs_to :user

  protected

  def strip_comma(s)
    if s.end_with? ','
      s.slice(0, s.length - 1)
    else
      s
    end
  end

  def convert(s)
    translation = ''
    s.each_char do |c|
      translation << "#{get_conversion(UnicodeUtils.downcase(c))}, "
    end
    strip_comma(translation.strip)
  end

  def translate
    self.content = convert(self.original)
  end
end
