class CreateTranslations < ActiveRecord::Migration
  def change
    create_table :translations do |t|
      t.string :original
      t.string :content

      t.timestamps null: false
    end
  end
end
