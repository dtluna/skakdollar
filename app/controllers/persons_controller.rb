class PersonsController < ApplicationController
  before_filter :authenticate_user!, only: :profile

  def profile
  end

  def index
    @users = User.includes(:translations).order(:email).page(params[:page])
  end
end
