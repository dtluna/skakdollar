class ChangeOriginalToText < ActiveRecord::Migration
  def change
    change_column :translations, :original, :text
  end
end
