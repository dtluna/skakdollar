module PersonsHelper
  def translations_count(user)
    user.translations.length
  end
end
